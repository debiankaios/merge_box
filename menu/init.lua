menu = {}
local menudata = {username = {text = ""}}

function menu.update(dt)
  if love.keyboard.isDown("lctrl") and love.keyboard.isDown("s") then
    if not merge_box.cache.save then
      merge_box.functions.save_menu()
      merge_box.cache.save = true
      merge_box.chat.send("Menu data were saved", "menu")
    end
  else
    if merge_box.cache.save then merge_box.cache.save = false end
  end

  suit.layout:reset(setX(5),setY(40))
	suit.Input(merge_box.menu.username, suit.layout:row(setX(130),setY(30)))
  suit.layout:reset(setX(5),setY(90))
  if suit.Button("Start Game!", suit.layout:row(setX(130),setY(30))).hit then
    merge_box.start = true
  end

  if merge_box.events.mouse_presed_in_area(680, 5, 696, 21, 1) then
    merge_box.menu.selected_profile = 1
  elseif merge_box.events.mouse_presed_in_area(680, 35, 696, 51, 1) then
    merge_box.menu.selected_profile = 2
  elseif merge_box.events.mouse_presed_in_area(680, 65, 696, 81, 1) then
    merge_box.menu.selected_profile = 3
  elseif merge_box.events.mouse_presed_in_area(680, 95, 696, 111, 1) then
    merge_box.menu.selected_profile = 4
  elseif merge_box.events.mouse_presed_in_area(680, 125, 696, 141, 1) then
    merge_box.menu.selected_profile = 5
  elseif merge_box.events.mouse_presed_in_area(680, 155, 696, 171, 1) then
    merge_box.menu.selected_profile = 6
  end
  suit.layout:row()
end

function menu.draw()
  merge_box.graphics.print("Username:", 5, 0, 0, 2, 2)
  merge_box.graphics.print("Profile 1", 700, 0, 0, 2, 2)
  merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 5, 0)
  merge_box.graphics.print("Profile 2", 700, 30, 0, 2, 2)
  merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 35, 0)
  merge_box.graphics.print("Profile 3", 700, 60, 0, 2, 2)
  merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 65, 0)
  merge_box.graphics.print("Profile 4", 700, 90, 0, 2, 2)
  merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 95, 0)
  merge_box.graphics.print("Profile 5", 700, 120, 0, 2, 2)
  merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 125, 0)
  merge_box.graphics.print("Profile 6", 700, 150, 0, 2, 2)
  merge_box.graphics.draw(merge_box.graphics.checkbox, 680, 155, 0)
  merge_box.graphics.draw(merge_box.graphics.red_x, 680, (merge_box.menu.selected_profile-1)*30+5, 0)
end
