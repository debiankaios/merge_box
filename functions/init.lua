require "functions/serialize"

-- Normal Window has x of 800
function merge_box.functions.setX(x)
  return x/800*love.graphics.getWidth()
end

-- Normal Window has y of 600
function merge_box.functions.setY(y)
  return y/600*love.graphics.getHeight()
end

-- Normal Window has x of 800
function setX(x)
  return x/800*love.graphics.getWidth()
end

-- Normal Window has y of 600
function setY(y)
  return y/600*love.graphics.getHeight()
end

function merge_box.functions.save_menu()
  local f = io.open(merge_box.save_dir.."menu.json","w")
  f:write(json.encode(merge_box.menu))
  f:close()
end
