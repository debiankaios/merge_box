function merge_box.register_box(boxname, def)
  table.insert(merge_box.boxes,{name=boxname,type=def.functional_type,stage=def.stage})
end

--[[
function merge_box.spawn_box(boxname, x, y)
  table.insert(merge_box.boxes_on_field,{name=boxname,type=def.functional_type,stage=def.stage})
end]]

--[[
TUTORIAL:
boxname = "merge_box:unknown"
def = {
imagename = loveimage
functional_type = false/"factory"/"housing"/"misc"
stage = {
  standard = 1
  min = 1
  max = 120
}
}
]]
