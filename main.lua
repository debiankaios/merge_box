json = require "json/json"
suit = require "suit"

merge_box = {}
merge_box.boxes = {}
merge_box.boxes_on_field = {}
merge_box.chat = {}
merge_box.cache = {}
merge_box.events = {}
merge_box.functions = {}
merge_box.game = {}
merge_box.graphics = {}
merge_box.menu = {}
merge_box.cache.save = false
merge_box.graphics.checkbox = love.graphics.newImage("img/checkbox_empty_white.png")
merge_box.graphics.red_x = love.graphics.newImage("img/symbol_x_red.png")
merge_box.version = 0.2
merge_box.start = false
merge_box.menu.selected_profile = 1
merge_box.menu.username = {text = ""}
if love.system.getOS() ~= "Windows" then
  merge_box.save_dir = love.filesystem.getAppdataDirectory().."merge_box/"
else
  merge_box.save_dir = love.filesystem.getAppdataDirectory()..[[/merge_box]]
end


require "menu"
require "game"
require "graphics"
require "functions"
require "events"

function love.quit()
  merge_box.functions.save_menu()
  return false
end

function love.load()
  if io.open(merge_box.save_dir..".folder.required") == nil then
    os.execute("mkdir  -p -- " .. merge_box.save_dir)
    local f = io.open(merge_box.save_dir..".folder.required","w")
    f:write("Path exist")
    f:close()
  end
  love.graphics.setNewFont("ttfs/LiberationSans-Regular.ttf")
  -- menut.txt is maybe not more supported in future
  local f = io.open(merge_box.save_dir.."menu.txt","r")
  if f then
    local table = merge_box.functions.deserialize(f:read("*all")) or {}
    merge_box.menu.selected_profile = tonumber(table["selected_profile"]) or 1
    merge_box.menu.username = table["username"] or {text = ""}
    f:close()
  end
  local f = io.open(merge_box.save_dir.."menu.json","r")
  if f then
    local table = json.decode(f:read("*all")) or {}
    merge_box.menu.selected_profile = tonumber(table["selected_profile"]) or 1
    merge_box.menu.username = table["username"] or {text = ""}
    f:close()
  end
end

function love.update(dt)
  menu.update(dt)
end

function love.draw()
  menu.draw()
  merge_box.chat.draw()
  suit.draw()
end

function love.textinput(t)
	suit.textinput(t)
end

function love.keypressed(key)
	suit.keypressed(key)
end
