ZIP = zip
zip_verbose = $(zip_verbose_$(V))
zip_verbose_ = $(zip_verbose_0)
zip_verbose_0 = @echo '  ZIP       ' $@;
zip_verbose_1 =

INSTALL = install

RM = rm

SED = sed

LOVE = love
love_verbose = $(love_verbose_$(V))
love_verbose_ = $(love_verbose_0)
love_verbose_0 = @echo '  LOVE      ' $<;
love_verbose_1 =


prefix = /usr/local

exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin

data_prefix = $(prefix)
datadir = $(data_prefix)/share
pkgdatadir = $(datadir)/$(PKGNAME)
applications_datadir = $(datadir)/applications
iconsdatadir = $(datadir)/icons
themeiconsdir = $(iconsdatadir)/hicolor
icon32x32dir = $(themeiconsdir)/32x32
apps32x32dir = $(icon32x32dir)/apps

PKGNAME = mergebox


# Default target
all : $(PKGNAME).love

.PHONY : install
install : support/mergebox.sh support/de.debiankaios.mergebox.desktop $(PKGNAME).love
	$(INSTALL) -Dm755 $(PKGNAME).love $(DESTDIR)$(pkgdatadir)/mergebox.love
	$(INSTALL) -Dm755 support/mergebox.sh $(DESTDIR)$(bindir)/mergebox
	$(INSTALL) -Dm755 support/de.debiankaios.mergebox.desktop $(DESTDIR)$(applications_datadir)/de.debiankaios.mergebox.desktop
	$(INSTALL) -Dm755 support/icon.png $(DESTDIR)$(apps32x32dir)/mergebox.png
	$(SED) -i 's+^\<love\>+'$(LOVE)'+;s+/usr/share/mergebox/mergebox\.love+'$(pkgdatadir)'/mergebox.love+' $(DESTDIR)$(bindir)/mergebox

.PHONY : uninstall
uninstall :
	-$(RM) -f -- $(DESTDIR)$(applications_datadir)/de.debiankaios.mergebox.desktop
	-$(RM) -f -- $(DESTDIR)$(bindir)/mergebox
	-$(RM) -f -- $(DESTDIR)$(pkgdatadir)/mergebox.love
	-$(RM) -f -- $(DESTDIR)$(apps32x32dir)/mergebox.png

$(PKGNAME).love : .
	$(zip_verbose) $(ZIP) -q -r $@ $<

.PHONY : run
run : .
	$(love_verbose) $(LOVE) $<
