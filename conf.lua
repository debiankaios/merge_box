function love.conf(t)
  t.identity = "merge_box"
  t.version = "11.3"
  t.window.title = "Merge Box"
  t.window.icon = "img/icon.png"
  t.window.resizable = true
  t.modules.math = false
  t.modules.physics = true
  t.modules.video = false
end
