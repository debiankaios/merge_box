function merge_box.graphics.draw(drawable, x, y, r, sx, sy, ox, oy, kx, ky)
  local x = x or 0
  local y = y or 0
  local r = r or 0
  local sx = sx or 1
  local sy = sy or sx
  local ox = ox or 0
  local oy = oy or 0
  local kx = kx or 0
  local ky = ky or 0
  love.graphics.draw(drawable, setX(x), setY(y), r, setX(sx), setY(sy), setX(ox), setY(oy), setX(kx), setY(ky))
end

function merge_box.graphics.print(text, x, y, r, sx, sy, ox, oy, kx, ky)
  local x = x or 0
  local y = y or 0
  local r = r or 0
  local sx = sx or 1
  local sy = sy or sx
  local ox = ox or 0
  local oy = oy or 0
  local kx = kx or 0
  local ky = ky or 0
  love.graphics.print(text, setX(x), setY(y), r, setX(sx), setY(sy), setX(ox), setY(oy), setX(kx), setY(ky))
end

require "graphics/chat"
