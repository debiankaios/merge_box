local menu_chat_msg = {"","","","","","",}

function merge_box.chat.send(message, where, player)
  if where == "menu" then
    table.insert(menu_chat_msg, message)
  end
end

function merge_box.chat.draw()
  merge_box.graphics.print(menu_chat_msg[#menu_chat_msg], 0, 390, 0, 2, 2)
  merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-1], 0, 420, 0, 2, 2)
  merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-2], 0, 450, 0, 2, 2)
  merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-3], 0, 480, 0, 2, 2)
  merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-4], 0, 510, 0, 2, 2)
  merge_box.graphics.print(menu_chat_msg[#menu_chat_msg-5], 0, 540, 0, 2, 2)
end
